@extends('website.index')
@section('main')
<section id="contact">
    <div class="hm-container">
        <div class="hm-row title">
            <div class="hm-col-12">
                <div class="contact-title"><h1>Thông tin về NPS</h1></div>
            </div>
        </div>
        <div class="hm-row top-contact">
            <div class="hm-col-12">
                <div class="hm-row hm-column-3">
                    <div class="item">
                        <div class="contact">
                            <div class="icon"><i class="fas fa-mobile-alt"></i></div>
                            <h3>Điện thoại</h3>
                            <p>Hotline: 0825.44.55.66</p>
                            <p>Điện thoại bàn: 028 7309 2939</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="contact">
                            <div class="icon"><i class="fas fa-map-marker-alt"></i></div>
                            <h3>Địa chỉ</h3>
                            <p>VP Giao dịch: 46 Dương Bá Trạc, Phường 2, Quận 8, Tp.HCM</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="contact">
                            <div class="icon"><i class="fas fa-envelope"></i></i></div>
                            <h3>Email</h3>
                            <p>Hỗ trợ kỹ thuật:NPS@nps.vn</p>
                            <p>Hỗ trợ: NPS@nps.vn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="form-contact">
    <div class="hm-row">
        <div class="hm-col-12">
            <div class="form-title">
                <h1>Liên hệ với chúng tôi</h1>
            </div>
        </div>
    </div>
    <div class="hm-container">
        <div class="hm-row">
            <div class="hm-col-12">
                <form action="">
                    <input type="text" name="name" id="name" placeholder="Họ và tên">
                    <input type="tel" name="phone-number" id="phone-number" placeholder="Số điện thoại">
                    <input type="email" name="mail" id="mail" placeholder="Email">
                    <textarea name="info" id="info" placeholder="Nhập nội dung liên hệ"></textarea>
                </form>
            </div>
        </div>
        <div class="hm-row button-social">
            <div class="hm-col-4">
                <div class="hm-row hm-column-2 list-button">
                    <div class="item">
                        <div class="button">
                            <button class="submit" type="submit">Gửi</button>
                        </div>
                    </div>
                    <div class="item">
                        <div class="button">
                            <button class="reset" type="submit">Hủy</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hm-col-8">
                <div class="socials">
                    <ul class="list-socials clear">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fas fa-envelope"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
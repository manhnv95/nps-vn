@php
    use App\Http\Controllers\Helper\MAC;
    $page = MAC::getPage();
    $seo = MAC::getSeoByPage(json_decode(@$page_data->meta));
    $widget_app = MAC::getConfig('app');
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ @$widget_app->favicon }}">
    <title>{{ @($page_title) ? $page_title : $widget_app->title_app }}</title>

    <link rel="stylesheet" type="text/css" href="website/css/firstpage.css">

{{-- SEO --}}
@foreach ($seo as $key => $value)
<! --------- SEO {{ $key }}------------- !>
{!! $value !!}
@endforeach
</head>
<body>
    <nav id="menu" class="head-nav">
        <div onclick="showMenu()" class="showmenu"></div>
        <div class="hm-container-fluid">
            <div class="hm-row va-m">
                <div class="hm-col-fluid-2 hm-push-fluid-2">
                    <a href="{{ route('home') }}" class="logo-nav"><img src="{{ @$widget_app->logo }}" alt=""></a>
                </div>
                <div class="hm-col-fluid-12 ta-r">
                    <ul class="menu">
                        @foreach ($page as $item)@if ($item->parent_id == 0 && !empty(json_decode($item->location)))@if (in_array('menu_top', json_decode($item->location)))<li><a @if($item->type != 'taxonomy'){{ 'href='.$item->slug.'.html' }}@else{{ (!empty($item->link)) ? 'href='.$item->link : '#' }}@endif>{{ $item->name }}</a>
                            @if (!empty($item->child))
                            <ul class="sub-menu">
                                @foreach (explode(',', $item->child) as $val)@if (json_decode(MAC::cateColInList($val, $page, 'display')) != 0)@if (in_array('menu_top', json_decode(MAC::cateColInList($val, $page, 'location'))))
                                <li><a href="{{ MAC::cateColInList($val, $page, 'slug') }}.html">{{ MAC::cateColInList($val, $page, 'name') }}</a></li>
                                @endif @endif @endforeach
                            </ul>
                            @endif</li>@endif @endif @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header>
        <!-- <img src="{{ @$widget_app->bg_slider }}"> -->
        <!-- <div class="hm-container-fluid">
            <div class="slide">
                <div class="slide-img active">
                    <img src="{{ @$widget_app->slider_img_1 }}" alt="">
                </div>
                <div class="slide-img">
                    <img src="{{ @$widget_app->slider_img_2 }}" alt="">
                </div>
                <div class="slide-img">
                    <img src="{{ @$widget_app->slider_img_3 }}" alt="">
                </div>
                <p class="title active">{{ @$widget_app->slider_text_1 }}</p>
                <p class="title">{{ @$widget_app->slider_text_2 }}</p>
                <p class="title">{{ @$widget_app->slider_text_3 }}</p>
                <a href="{{ @$widget_app->slider_button_link_1 }}" class="buy active">{{ @$widget_app->slider_button_1 }}</a>
                <a href="{{ @$widget_app->slider_button_link_2 }}" class="buy">{{ @$widget_app->slider_button_2 }}</a>
                <a href="{{ @$widget_app->slider_button_link_3 }}" class="buy">{{ @$widget_app->slider_button_3 }}</a>
            </div>
            <div class="slide-img">
                <img src="website/images/img_slide/slide-1" alt="">
                <img src="website/images/img_slide/slide-2" alt="">
                <img src="website/images/img_slide/slide-3" alt="">
            </div>
            <div class="wrapper-indicators">
                <div class="indicator active"></div>
                <div class="indicator"></div>
                <div class="indicator"></div>
            </div>
        </div> -->
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
        </div>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <a href="{{ @$widget_app->slider_button_link_1 }}" class="carousel-item active">
                    <img class="d-block w-100" src="{{ @$widget_app->slider_img_1 }}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>{{ @$widget_app->slider_title_1 }}</h5>
                        <p>{{ @$widget_app->slider_text_1 }}</p>
                        @if(@$widget_app->slider_button_1)
                        <a href="{{ @$widget_app->slider_button_link_1 }}">{!! @$widget_app->slider_button_1 !!}</a>
                        @endif
                    </div>
                </a>
                <a href="{{ @$widget_app->slider_button_link_2 }}" class="carousel-item">
                    <img class="d-block w-100" src="{{ @$widget_app->slider_img_2 }}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>{{ @$widget_app->slider_title_2 }}</h5>
                        <p>{{ @$widget_app->slider_text_2 }}</p>
                        @if(@$widget_app->slider_button_2)
                        <a href="{{ @$widget_app->slider_button_link_2 }}">{!! @$widget_app->slider_button_2 !!}</a>
                        @endif
                    </div>
                </a>
                <a href="{{ @$widget_app->slider_button_link_3 }}" class="carousel-item">
                    <img class="d-block w-100" src="{{ @$widget_app->slider_img_3 }}" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>{{ @$widget_app->slider_title_3 }}</h5>
                        <p>{{ @$widget_app->slider_text_3 }}</p>
                        @if(@$widget_app->slider_button_3)
                        <a href="{{ @$widget_app->slider_button_link_3 }}">{!! @$widget_app->slider_button_3 !!}</a>
                        @endif
                    </div>
                </a>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
    <main>
        @yield('main')
    </main>
    <footer id="footer">
        <div class="hm-container-fluid">
            <div class="hm-container">
                <div class="hm-row">
                    <div class="hm-col-8">
                        <div class="widgets hm-row hm-column-3">
                            <div class="item">
                                <div class="widget widget-company">
                                    <h3 class="widget-title">{{ @$widget_app->footer_col1_title }}</h3>
                                    <p>{!! @$widget_app->footer_col1_content !!}</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="widget widget-info">
                                    <h3 class="widget-title">{{ @$widget_app->footer_col2_title }}</h3>
                                    <p>{!! @$widget_app->footer_col2_content !!}</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="widget widget-menu">
                                    <h3 class="widget-title">Đối tác</h3>
                                    <ul>
                                        {{-- @foreach ($page as $item)
                                        @if (in_array('footer', json_decode($item->location))) --}}
                                        <li><a href="https://www.nps.vn/hop-tac-chien-luoc-giua-nps-va-dai-hoc-tai-nguyen-moi-truong.html">Đại Học Tài Nguyên & Môi Trường</a></li>
                                        <li><a href="https://www.nps.vn/hop-tac-chien-luoc-giua-nps-va-dao-tao-tin-hoc.html">Đào Tạo Tin Học</a></li>
                                        {{-- @endif
                                        @endforeach --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hm-col-4" style="margin-right: -8.333%">
                        <div class="contact">
                            <p>{!! @$widget_app->footer_company_info !!}</p>
                            <img src="{{ @$widget_app->footer_img1 }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="list-payments hm-row va-m">
                    <div class="hm-col-2">
                        <div class="payment">
                            <img src="" alt="">
                        </div>
                    </div>
                    <div class="hm-col-2">
                        <div class="payment">
                            <img src="{{ @$widget_app->footer_img2 }}" alt="">
                        </div>
                    </div>
                    <div class="hm-col-2">
                        <div class="payment">
                            <img src="{{ @$widget_app->footer_img3 }}" alt="">
                        </div>
                    </div>
                    <div class="hm-col-2">
                        <div class="payment">
                            <img src="{{ @$widget_app->footer_img4 }}" alt="">
                        </div>
                    </div>
                    <div class="hm-col-3 hm-pull-1">
                        <a target="_blank" href="{{ @$widget_app->bct_href }}"><img  src="{{ @$widget_app->footer_img5 }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <i class="fas fa-angle-double-down"></i>
    </footer>
    @if (empty(Request::segment(1)))
    <div class="direc">
        <a href="#our-product" class="img"><i class="fas fa-address-book"></i> <span>Our Product</span></a>
        <a href="#hosting" class="img"><i class="fas fa-hdd"></i> <span>Hosting</span></a>
        <a href="#vps" class="img"><i class="fas fa-cloud"></i> <span>VPS</span></a>
        <a href="#server" class="img"><i class="fas fa-server"></i> <span>Thuê Server</span></a>
        <a href="#firewall" class="img"><i class="fab fa-gripfire"></i> <span>Firewall </span></a>
        <a href="#sellserver" class="img"><i class="fas fa-cloud"></i> <span>Bán Server</span></a>
        <a href="#serverplace" class="img"><i class="fas fa-map-marker-alt"></i> <span>Chỗ Đặt Server</span></a>
        <a href="#rack" class="img"><i class="fas fa-coins"></i> <span>Thuê Tủ Rack</span></a>
    </div>
    <a id="arrow-direc"><i class="fas fa-caret-right fa-2x"></i></a>
    @endif
    <div id="messeger">
        <a href="http://m.me/350741888430166" target="_blank"><i class="fab fa-facebook-messenger"></i></a>
    </div>
</body>
    <link rel="icon" href="{{ @$widget_app->favicon }}">
    <link href="{{ @$widget_app->font_app }}" rel="stylesheet">
    <link rel="stylesheet" href="website/css/fontawesome.css">
    <link rel="stylesheet" href="website/css/style.css">
    <link rel="stylesheet" href="website/css/responsive.css">

    <script type="text/javascript">

        var screenWidth = screen.availWidth,
            body = document.querySelector("body"),
            messeger = document.getElementById("messeger");
        if(screenWidth > 1024){
            var tweenMax = document.createElement("script");
                tweenMax.setAttribute("src","website/js/TweenMax.min.js");

            var script = document.createElement("script");
            script.setAttribute("src", "website/js/wow.min.js");
            var link = document.createElement("link");
            link.setAttribute("rel","stylesheet")
            link.setAttribute("type","text/css")
            link.setAttribute("href","website/css/animate.css");
            document.querySelector("head").append(link);
            body.append(tweenMax);
            body.append(script);
        }
        var mainJS = document.createElement("script");
            mainJS.setAttribute("src","website/js/main.js");
            body.append(mainJS);
        if(screenWidth > 1024){

            window.fbAsyncInit = function() {
            FB.init({
              xfbml            : true,
              version          : 'v3.3'
            });
          };
              (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }

        if(screenWidth <= 1024){
            messeger.style.display = "block";
        }
        messeger.firstElementChild.addEventListener("click",function(){
            window.location="https://www.facebook.com/antiddosvn/";
        })
    </script>

    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="350741888430166"
      theme_color="#0084ff"
      logged_in_greeting="NPS xin chào bạn, bạn cần hỗ trợ hay tư vấn dịch vụ, hãy chat với chúng tôi"
      logged_out_greeting="NPS xin chào bạn, bạn cần hỗ trợ hay tư vấn dịch vụ, hãy chat với chúng tôi">
    </div>
    <script>
        var getOS = navigator.userAgent.toLowerCase(),
        img = document.querySelectorAll("img");
        if(getOS.indexOf("iphone") != -1 || getOS.indexOf("os") != -1){
            img.forEach(function(el, index){
                var linkImg = el.getAttribute("src"),
                    webP = linkImg.lastIndexOf("webp");
                if(webP != -1){

                    var reLink = linkImg.replace("webp","png");
                    el.setAttribute("src", reLink);
                }

        })
    }
    </script>
    <!-- <script src="website/js/TweenMax.min.js"></script>
    <script src="website/js/slide.js"></script> -->
    <!-- bootstrap -->
    <link rel="stylesheet" href="website/css/bootstrap.min.css">
    <script src="website/js/jquery-3.3.1.slim.min.js"></script>
    <script src="website/js/popper.min.js"></script>
    <script src="website/js/bootstrap.min.js"></script>
    

</html>
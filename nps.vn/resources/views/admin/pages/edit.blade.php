@php
	use App\Http\Controllers\Helper\MAC;
@endphp
@extends('admin.home')
@section('content')
<script src="/ckeditor/ckeditor.js"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Cập nhật trang</h4>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<form action="" method="post" accept-charset="utf-8">
						@csrf
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<label class="label_mac">Name</label>
								<input type="text" name="name" class="form-control input_mac" value="{{ $data->name }}" required="" maxlength="200">
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<label class="label_mac">Link</label>
								<input type="text" name="slug" class="form-control input_mac" value="{{ $data->slug }}" disabled="">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<select name="parent" class="form-control select_mac">
									<option value="0" >No Parent</option>
									@foreach ($list as $item)
										@if ($item->parent_id == 0 && $item->id != $data->id)
										<option value="{{ $item->id }}" {{ @($item->id == $data->parent_id) ? 'selected' : '' }}>{{ $item->name }}</option>
										@endif
									@endforeach
								</select>
							</div>
							<div class="col-md-6 pr-0 form_mac">
								<select name="display" class="form-control select_mac">
									<option value="0" {{ (@$data->display == 0) ? 'selected' : '' }}>Hide</option>
									<option value="1" {{ (@$data->display == 1) ? 'selected' : '' }}>Show</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 form_mac">
								<select name="type" class="form-control select_mac">
									<option value="0" >--- SELECT TYPE ---</option>
									@foreach (config('page.type') as $key => $val)
										<option value="{{ $key }}" {{ (@$data->type == $key) ? 'selected' : '' }}>{{ $val }}</option>
									@endforeach
								</select>
							</div>
							@if (@$data->type == 'page')
							<div class="col-md-6 pr-0 form_mac">
								<select name="html" class="form-control select_mac">
									<option value="" >--- SELECT HTML ---</option>
									@foreach (config('page.html') as $key => $val)
										<option value="{{ $key }}" {{ (@$data->html == $key) ? 'selected' : '' }}>{{ $val }}</option>
									@endforeach
								</select>
							</div>
							@elseif (@$data->type == 'post')
							<div class="col-md-6 pr-0 form_mac">
								<select name="taxonomy" class="form-control select_mac">
									<option value="0" >--- SELECT CATEGORY ---</option>
									@foreach ($category as $item)
										<option value="{{ $item->id }}" {{ (@$data->taxonomy == $item->id) ? 'selected' : '' }}>{{ $item->name }}</option>
									@endforeach
								</select>
							</div>
							@elseif (@$data->type == 'product')
							<div class="col-md-6 pr-0 form_mac">
								<select name="taxonomy" class="form-control select_mac">
									<option value="0" >--- SELECT CATALOG ---</option>
									@foreach ($catalog as $item)
										<option value="{{ $item->id }}" {{ (@$data->taxonomy == $item->id) ? 'selected' : '' }}>{{ $item->name }}</option>
									@endforeach
								</select>
							</div>
							@endif
						</div>
						<div class="form-group row">
							<div class="col-md-6 pl-0 checkbox_mac">
								@foreach (config('location.taxonomy') as $item)
								<div class="custom-control custom-checkbox d-inline pr-5">
									<input type="checkbox" class="custom-control-input" name="location[]" value="{{ $item }}" id="{{ $item }}_checkbox" {{ (in_array($item, json_decode($data->location))) ? 'checked' : '' }}>
									<label class="custom-control-label" for="{{ $item }}_checkbox">{{ strtoupper($item) }}</label>
								</div>
								@endforeach
							</div>
							<div class="col-md-3 form_mac">
								<label class="label_mac">Độ ưu tiên</label>
								<input type="text" name="priority" class="form-control input_mac" value="{{ $data->priority }}">
							</div>
							<div class="col-md-3 pr-0 form_mac">
								<label class="label_mac">Số thứ tự</label>
								<input type="number" step="any" name="stt" class="form-control input_mac" value="{{ $data->stt }}">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								<label class="label_mac">Tiêu đề trang</label>
								<input type="text" name="page_title" class="form-control input_mac" value="{{ @$data->page_title }}">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0 form_mac">
								<label class="label_mac">Đường dẫn trang</label>
								<input type="text" name="link" class="form-control input_mac" value="{{ @$data->link }}">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12 px-0">
								<label class="">Nội dung trang</label>
								<textarea id="editor" name="content" class="form-control">{{ @$data->content }}</textarea>
							</div>
						</div>
						<div id="html_seo">
							@if (!empty(json_decode(@$data->meta)))
							@php $str_seo = ''; @endphp
							@foreach (json_decode(@$data->meta) as $key => $val)
								@php $str_seo = $str_seo.','.$key @endphp
								<div class="form-group row">
									<div class="col-md-12 px-0">
										<label class=""><button type="button" class="btn btn-danger btn_del_seo" onclick="return confirm('Bạn có chắn chắn muốn xóa thẻ SEO: {{ $key }} này không?');"><i class="fas fa-minus"></i></button><span class="pl-2">Nội dung thẻ SEO: {{ $key }}</span></label>
										<textarea class="form-control" name="{{ $key }}" rows="5">{!! $val !!}</textarea>
									</div>
								</div>
							@endforeach
							@endif
						</div>
						<div class="form-group row">
							<div class="col-md-2 pl-0">
								<button type="button" class="btn btn-primary" id="btn_add_seo"><i class="fas fa-plus"></i></button>
							</div>
							<div class="col-md-10 pr-0 form_mac">
								<label class="label_mac">Tên thẻ SEO (* phải cùng tên với thẻ SEO muốn thay thế ngoài trang chủ và không nên có ký tự đặc biệt)</label>
								<input type="text" id="name_seo" class="form-control input_mac" value="">
								<input type="text" id="cate_child" name="cate_child" class="form-control input_mac" value="">
								<input type="text" value="{{ @$str_seo }}" name="str_seo" id="str_seo" class="d-none">
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn">Cập nhật</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@if (!empty($child))
	<div class="row pt-5">
		<div class="col-md-3">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Danh sách danh mục con</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					@foreach ($child as $item)
					<div class="custom-control custom-checkbox pr-5">
						<input type="checkbox" class="custom-control-input child_checkbox" value="{{ $item }}" id="{{ $item }}_checkbox" checked="">
						<label class="custom-control-label" for="{{ $item }}_checkbox">{{ strtoupper(MAC::cateColInList($item, $list, 'name')) }}</label>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
<style type="text/css" media="screen">
	.btn_del_seo{
		padding: 0 5px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('[name="name"]').keyup(function() {
			slug = $('[name="name"]').val();
			slug = replace_vi(slug);
			slug = slug.replace(/\s/gi,'-');
			$('[name="slug"]').val(slug);
		});
		$('.child_checkbox').change(function() {
			all_checkbox = $('.child_checkbox:checked');
			arr = [];
			all_checkbox.each(function() {
				arr.push($(this).val());
			});
			console.log(arr);
			$('[name="cate_child"]').val(arr);
		});

		$('#btn_add_seo').click(function () {
			name_seo = $('#name_seo').val();
			name_seo = replace_vi(name_seo);
			name_seo = name_seo.replace(/\s/gi,'_');
			str_seo = $('[name="str_seo"]').val();

			if (name_seo == "") {
				alert('Vui lòng điền tên cho thẻ SEO muốn thêm!');
			}else{
				name_seo = 'seo_'+name_seo;
				str_seo = $('[name="str_seo"]').val();
				n = str_seo.indexOf(name_seo);
				if (n == -1) {
					str_seo += ','+name_seo;
					// $('[name="str_seo"]').val(str_seo);
					str_seo = $('[name="str_seo"]').attr('value', str_seo);
					html = `<div class="form-group row">
								<div class="col-md-12 px-0">
									<label class=""><button type="button" class="btn btn-danger btn_del_seo" onclick="return confirm('Bạn có chắn chắn muốn xóa thẻ SEO: `+name_seo+` này không?');"><i class="fas fa-minus"></i></button><span class="pl-2">Nội dung thẻ SEO: `+name_seo+`</span></label>
									<textarea class="form-control" name="`+name_seo+`" rows="5"></textarea>
								</div>
							</div>`;
					$('#html_seo').append(html);
					$('#name_seo').val("");
				}else{
					alert('Thẻ SEO '+name_seo+' đã tồn tại, vui lòng chọn tên khác!');
				}
			}
		});
		del_seo();
		var btnAdd = document.getElementById("btn_add_seo");
		btnAdd.addEventListener("click",function(){
			del_seo();
		});
		function del_seo(){
			var delBtn = Array.from(document.querySelectorAll(".btn_del_seo")),
			strSeoVal = document.getElementById("str_seo");
			delBtn.forEach(function(el){
				el.addEventListener("click",function(){
					var getName = el.parentElement.parentElement.parentElement.firstElementChild.lastElementChild.getAttribute("name");
					el.parentElement.parentElement.parentElement.remove();
					var newValue = strSeoVal.getAttribute("value").replace(getName,"");
					strSeoVal.setAttribute("value",newValue);

				});
			});

		}

	});

	CKEDITOR.replace('editor', {
		height: 700,
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
</script>
@endsection
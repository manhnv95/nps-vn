@php
	use App\Http\Controllers\Helper\MAC;
@endphp
@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-4 card_title">
							<h4>Danh sách danh mục sản phẩm</h4>
						</div>
						<div class="col-md-4 text-center btn_add">
							<a href="{{ route('admin.catalog.add') }}"><i class="fas fa-plus"></i></a>
						</div>
						<div class="col-md-4 card_search">
							<form action="" method="post" accept-charset="utf-8" class="">
								@csrf
								<div class="input-group justify-content-end">
									<input type="text" class="form-control input_search" name="" value="" placeholder="Search">
									<button type="submit" class="btn btn-white"><i class="fas fa-search"></i></button>
								</div>
							</form>
						</div>
					</div>
				</div>
				@include('admin.notify')
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Parent</th>
									<th>Count Product</th>
									<th>Created Date</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach (@$list as $item)
								<tr>
									<td>{{ @$item->id }}</td>
									<td><a href="{{ route('admin.catalog.edit', ['id' => $item->id]) }}" class="px-0">{{ @$item->name }}</a></td>
									<td>{{ MAC::cateColInList($item->parent_id, $list, 'name') }}</td>
									<td>{{ @$item->count_product }}</td>
									<td>{{ @$item->created_at }}</td>
									<td>
										<a href="{{ route('admin.catalog.edit', ['id' => $item->id]) }}"><i class="fas fa-edit"></i></a>
										<a href="{{ route('admin.catalog.delete', ['id' => $item->id]) }}" onclick="return confirm('Bạn có chắn chắn muốn xóa danh mục sản phẩm: {{ @$item->name }} không?');"><i class="fas fa-trash-alt"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $list->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row pt-5">
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Menu Top</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					@foreach ($list as $item)
						@if ($item->display == 1 && @in_array('menu_top', json_decode($item->location)) && $item->parent_id == 0)
							<div class="category_parent category_show">
								<p>{{ $item->name }}</p>
							</div>
							@if (!empty($item->child))
								@foreach (explode(',', $item->child) as $val)
									<div class="category_child category_show">
										<p>{{ MAC::cateColInList($val, $list, 'name') }}</p>
									</div>
								@endforeach
							@endif
						@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Sidebar</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					@foreach ($list as $item)
						@if ($item->display == 1 && @in_array('sidebar', json_decode($item->location)) && $item->parent_id == 0)
							<div class="category_parent category_show">
								<p>{{ $item->name }}</p>
							</div>
							@if (!empty($item->child))
								@foreach (explode(',', $item->child) as $val)
									<div class="category_child category_show">
										<p>{{ MAC::cateColInList($val, $list, 'name') }}</p>
									</div>
								@endforeach
							@endif
						@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Footer</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					@foreach ($list as $item)
						@if ($item->display == 1 && @in_array('footer', json_decode($item->location)) && $item->parent_id == 0)
							<div class="category_parent category_show">
								<p>{{ $item->name }}</p>
							</div>
							@if (!empty($item->child))
								@foreach (explode(',', $item->child) as $val)
									<div class="category_child category_show">
										<p>{{ MAC::cateColInList($val, $list, 'name') }}</p>
									</div>
								@endforeach
							@endif
						@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
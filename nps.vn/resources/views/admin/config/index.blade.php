@php
	use App\Http\Controllers\Helper\MAC;
@endphp
@extends('admin.home')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-12 card_title">
							<h4>Giao diện</h4>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<a href="{{ route('admin.config.show_homepage') }}" title="">Trang chủ</a>
						</div>
						<div class="col-md-6 text-right">
							<a href="{{ route('admin.config.show_homepage') }}" title=""><i class="fas fa-edit"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
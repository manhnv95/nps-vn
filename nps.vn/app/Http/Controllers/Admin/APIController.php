<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class APIController extends Controller
{
    public function index()
    {
    	$hosting = DB::table('hosting')->get();
    	$data = [
    		'status' 	=> 200,
    		'data'		=> $hosting,
    	];
    	return $data;
    }

    public function store(Request $request)
    {
    	$date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     => (string) $request->name,
            'catalog'  => $catalog,
            'fees'     => (int) $request->fees,
            'periodic' => (int) $request->periodic,
            'display'  => (int) $request->display,
            'note'     => (string) $request->note,
            'link'     => (string) $request->link,
            'storage'  => (string) $request->storage,
            'bandwidth'     => (string) $request->bandwidth,
            'domain'        => (string) $request->domain,
            'sub_domain'    => (string) $request->sub_domain,
            'pack_domain'   => (string) $request->pack_domain,
            'addon_domain'  => (string) $request->addon_domain,
            'database'      => (int) $request->database,
            'ftp_account'   => (int) $request->ftp_account,
            'email'  => (int) $request->email,
            'ssl'    => (string) $request->ssl,
            'ip'     => (int) $request->ip,
            'services'   => (string) $request->services,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('hosting')->insertGetId($data);

        if ($id) {
        	
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới danh mục sản phẩm thành công!');
            return redirect()->route('admin.catalog.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới danh mục sản phẩm không thành công!');
            return redirect()->route('admin.catalog.index');
        }
    }
}

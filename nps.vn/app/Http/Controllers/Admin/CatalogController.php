<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Illuminate\Support\Str;

class CatalogController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('catalog')->paginate(10);
        return view('admin.catalog.index', $data);
    }

    public function create()
    {
    	$data['list'] = DB::table('catalog')->select('id', 'name', 'parent_id', 'display')->get();
        return view('admin.catalog.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:200'
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $data = [
            'name'     => $request->name,
            'slug'     => Str::slug($request->name, '-'),
            'parent_id' => $parent_id,
            'display'     => $request->display,
            'type'     => $request->type,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $check_slug = DB::table('catalog')->where('slug', $data['slug'])->first();
		if (isset($check_slug)) {
			Session::flash('status', 'danger');
            Session::flash('notify', 'Danh mục này đã tồn tại, vui lòng chọn tên khác!');
            return redirect()->route('admin.catalog.add');
		}
        $id = DB::table('catalog')->insertGetId($data);

        if ($id) {
        	if ($parent_id != 0) {
	    		$arr_cate = DB::table('catalog')->where('id', $parent_id)->value('child');
				$arr_cate = explode(',', $arr_cate);
				if (empty($arr_cate)) {
					$catalog_child = [$id];
				}else{
					array_push($arr_cate, $id);
					$catalog_child = $arr_cate;
				}
				$catalog_child = array_unique($catalog_child);
				$catalog_child = implode(',', $catalog_child);
				DB::table('catalog')->where('id', $parent_id)->update(['child' => $catalog_child]);
	    	}
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới danh mục sản phẩm thành công!');
            return redirect()->route('admin.catalog.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới danh mục sản phẩm không thành công!');
            return redirect()->route('admin.catalog.index');
        }
    }

    public function edit($id)
    {
        $cate = DB::table('catalog')->where('id', $id)->first();
        if (isset($cate)) {
            $data['data'] = $cate;
            $data['list'] = DB::table('catalog')->get();
			if ($cate->child) {
				$data['child'] = explode(',', $cate->child);
			}
            return view('admin.catalog.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.catalog.index');
    }

    public function update(Request $request, $id)
    {
       $cate = DB::table('catalog')->where('id', $id)->first();
        if (isset($cate)) {
            $rules = [
                'name'              => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $data = [
	            'name'     	=> $request->name,
	            'slug'     	=> Str::slug($request->name, '-'),
	            'parent_id' => $parent_id,
	            'display'   => $request->display,
                'type'     => $request->type,
	            'child'		=> $request->cate_child,
	            'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('catalog')->where('id', $id)->update($data);
            if ($function) {
            	if ($parent_id != 0) {
		    		$arr_cate = DB::table('catalog')->where('id', $parent_id)->value('child');
					$arr_cate = explode(',', $arr_cate);
					if (empty($arr_cate)) {
						$catalog_child = [$id];
					}else{
						array_push($arr_cate, $id);
						$catalog_child = $arr_cate;
					}
					$catalog_child = array_unique($catalog_child);
					$catalog_child = implode(',', $catalog_child);
					DB::table('catalog')->where('id', $parent_id)->update(['child' => $catalog_child]);
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin danh mục thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin danh mục không thành công!');
            }
            return redirect()->route('admin.catalog.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.catalog.index');
    }

    public function delete($id)
    {
        $catalog = DB::table('catalog')->where('id', $id)->where('count_product', 0)->first();
        if (isset($catalog)) {
        	$child = explode(',', $catalog->child);
			if (isset($child)) {
				Session::flash('status', 'success');
                Session::flash('notify', 'Vui lòng di chuyển những danh mục con ra khỏi danh mục này rồi hãy xóa!');
				return redirect()->route('admin.catalog.index');
			}
            $function = DB::table('catalog')->where('id', $id)->delete();
            if ($function) {
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa danh mục sản phẩm thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa danh mục sản phẩm không thành công!');
            }
            return redirect()->route('admin.catalog.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có danh mục này!');
        return redirect()->route('admin.catalog.index');
    }
}

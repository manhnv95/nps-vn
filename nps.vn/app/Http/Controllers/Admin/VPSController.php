<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class VPSController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('vps')->paginate(10);
        return view('admin.vps.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.vps.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:250|unique:vps,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     => (string) $request->name,
            'catalog'  => $catalog,
            'fees'     => (int) $request->fees,
            'periodic' => (int) $request->periodic,
            'display'  => (int) $request->display,
            'note'     => (string) $request->note,
            'cpu'  => (string) $request->cpu,
            'ram'  => (string) $request->ram,
            'disk'  => (string) $request->disk,
            'storage'  => (string) $request->storage,
            'speed'  => (string) $request->speed,
            'bandwidth'     => (string) $request->bandwidth,
            'OS'        => (string) $request->os,
            'services'   => (string) $request->services,
            'link'     => (string) $request->link,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('vps')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới vps thành công!');
            return redirect()->route('admin.vps.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới vps không thành công!');
            return redirect()->route('admin.vps.index');
        }
    }

    public function edit($id)
    {
        $vps = DB::table('vps')->where('id', $id)->first();
        if (isset($vps)) {
            $data['data'] = $vps;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.vps.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có vps này!');
        return redirect()->route('admin.vps.index');
    }

    public function update(Request $request, $id)
    {
       $vps = DB::table('vps')->where('id', $id)->first();
        if (isset($vps)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($vps->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'     => (string) $request->name,
	            'catalog'  => $catalog,
	            'fees'     => (int) $request->fees,
	            'periodic' => (int) $request->periodic,
	            'display'  => (int) $request->display,
	            'note'     => (string) $request->note,
	            'cpu'  => (string) $request->cpu,
	            'ram'  => (string) $request->ram,
	            'disk'  => (string) $request->disk,
	            'storage'  => (string) $request->storage,
	            'speed'  => (string) $request->speed,
	            'bandwidth'     => (string) $request->bandwidth,
	            'OS'        => (string) $request->os,
	            'services'   => (string) $request->services,
                'link'     => (string) $request->link,
                'location'   => $location,
	            'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('vps')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $vps->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin vps thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin vps không thành công!');
            }
            return redirect()->route('admin.vps.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có vps này!');
        return redirect()->route('admin.vps.index');
    }

    public function delete($id)
    {
        $vps = DB::table('vps')->where('id', $id)->first();
        if (isset($vps)) {
            $function = DB::table('vps')->where('id', $id)->delete();
            if ($function) {
                if ($vps->catalog != 0) {
                    DB::table('catalog')->where('id', $vps->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa vps thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa vps không thành công!');
            }
            return redirect()->route('admin.vps.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có vps này!');
        return redirect()->route('admin.vps.index');
    }
}

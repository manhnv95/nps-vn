<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;

class HostingController extends Controller
{
    public function index()
    {
        $data['list'] = DB::table('hosting')->paginate(10);
        return view('admin.hosting.index', $data);
    }

    public function create()
    {
        $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
        return view('admin.hosting.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name'              => 'required|max:250|unique:hosting,name',
        ];

        $messages = config('lang.vi');
        $this->validate($request, $rules, $messages);

        $date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
        $catalog = (int) $request->catalog;
        $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
        $data = [
            'name'     => (string) $request->name,
            'catalog'  => $catalog,
            'fees'     => (int) $request->fees,
            'periodic' => (int) $request->periodic,
            'display'  => (int) $request->display,
            'note'     => (string) $request->note,
            'link'     => (string) $request->link,
            'storage'  => (string) $request->storage,
            'bandwidth'     => (string) $request->bandwidth,
            'domain'        => (string) $request->domain,
            'sub_domain'    => (string) $request->sub_domain,
            'pack_domain'   => (string) $request->pack_domain,
            'addon_domain'  => (string) $request->addon_domain,
            'database'      => (int) $request->database,
            'ftp_account'   => (int) $request->ftp_account,
            'email'  => (int) $request->email,
            'ssl'    => (string) $request->ssl,
            'ip'     => (int) $request->ip,
            'services'   => (string) $request->services,
            'location'   => $location,
            'created_at' => $date_time,
            'updated_at' => $date_time,
        ];
        $id = DB::table('hosting')->insertGetId($data);

        if ($id) {
            if ($catalog != 0) {
                DB::table('catalog')->where('id', $catalog)->increment('count_product');
            }
            Session::flash('status', 'success');
            Session::flash('notify', 'Thêm mới hosting thành công!');
            return redirect()->route('admin.hosting.edit', ['id' => $id]);
        }else{
            Session::flash('status', 'danger');
            Session::flash('notify', 'Thêm mới hosting không thành công!');
            return redirect()->route('admin.hosting.index');
        }
    }

    public function edit($id)
    {
        $hosting = DB::table('hosting')->where('id', $id)->first();
        if (isset($hosting)) {
            $data['data'] = $hosting;
            $data['catalog'] = DB::table('catalog')->select('id', 'name')->where('display', 1)->get();
            return view('admin.hosting.edit', $data);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có hosting này!');
        return redirect()->route('admin.hosting.index');
    }

    public function update(Request $request, $id)
    {
       $hosting = DB::table('hosting')->where('id', $id)->first();
        if (isset($hosting)) {
            $rules = [
                'name' => 'required|max:200',
            ];
            $messages = config('lang.vi');
            $date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
            $catalog = (int) $request->catalog;
            $catalog_new = 0;
            if ($hosting->catalog != $catalog) {
                $catalog_new = $catalog;
            }
            $location = (!empty($request->location)) ? json_encode($request->location) : json_encode(['page']);
            $data = [
	            'name'     => (string) $request->name,
                'catalog'  => ($catalog_new) ? $catalog_new : $hosting->catalog,
                'fees'     => (int) $request->fees,
                'periodic' => (int) $request->periodic,
                'display'  => (int) $request->display,
                'note'     => (string) $request->note,
                'link'     => (string) $request->link,
                'storage'  => (string) $request->storage,
                'bandwidth'     => (string) $request->bandwidth,
                'domain'        => (string) $request->domain,
                'sub_domain'    => (string) $request->sub_domain,
                'pack_domain'   => (string) $request->pack_domain,
                'addon_domain'  => (string) $request->addon_domain,
                'database'      => (int) $request->database,
                'ftp_account'   => (int) $request->ftp_account,
                'email'  => (int) $request->email,
                'ssl'    => (string) $request->ssl,
                'ip'     => (int) $request->ip,
                'services'   => (string) $request->services,
                'location'   => $location,
                'updated_at' => $date_time,
	        ];

            $this->validate($request, $rules, $messages);
            $function = DB::table('hosting')->where('id', $id)->update($data);
            if ($function) {
            	if ($catalog_new != 0) {
		    		DB::table('catalog')->where('id', $catalog_new)->increment('count_product');
                    DB::table('catalog')->where('id', $hosting->catalog)->decrement('count_product');
		    	}
                Session::flash('status', 'success');
                Session::flash('notify', 'Cập nhật thông tin hosting thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Cập nhật thông tin hosting không thành công!');
            }
            return redirect()->route('admin.hosting.edit', ['id' => $id]);
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có hosting này!');
        return redirect()->route('admin.hosting.index');
    }

    public function delete($id)
    {
        $hosting = DB::table('hosting')->where('id', $id)->first();
        if (isset($hosting)) {
            $function = DB::table('hosting')->where('id', $id)->delete();
            if ($function) {
                if ($hosting->catalog != 0) {
                    DB::table('catalog')->where('id', $hosting->catalog)->decrement('count_product');
                }
                Session::flash('status', 'success');
                Session::flash('notify', 'Xóa hosting thành công!');
            }else{
                Session::flash('status', 'danger');
                Session::flash('notify', 'Xóa hosting không thành công!');
            }
            return redirect()->route('admin.hosting.index');
        }
        Session::flash('status', 'danger');
        Session::flash('notify', 'Không có hosting này!');
        return redirect()->route('admin.hosting.index');
    }
}

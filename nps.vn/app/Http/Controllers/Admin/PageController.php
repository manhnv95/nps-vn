<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\MAC;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;

class PageController extends Controller {
	public function index() {
		$data['list'] = DB::table('pages')->paginate(10);
		return view('admin.pages.index', $data);
	}

	public function create() {
		$data['list'] = DB::table('pages')->select('id', 'name', 'parent_id')->where('display', 1)->get();
		return view('admin.pages.add', $data);
	}

	public function store(Request $request) {
		$rules = [
			'name' => 'required|max:200',
		];

		$messages = config('lang.vi');
		$this->validate($request, $rules, $messages);

		$date_time = date('Y-m-d H:i:s');
		$parent_id = (int) $request->parent;
		$location = (!empty($request->location)) ? json_encode($request->location) : json_encode([]);
		$data = [
			'name' => $request->name,
			'slug' => Str::slug($request->name, '-'),
			'display' => $request->display,
			'location' => $location,
			'stt' => (int) $request->stt,
			'type' => empty($request->type) ? 'taxonomy' : $request->type,
		];
		$check_slug = DB::table('pages')->where('slug', $data['slug'])->first();
		if (isset($check_slug)) {
			Session::flash('status', 'danger');
			Session::flash('notify', 'Trang này đã tồn tại, vui lòng chọn tên khác!');
			return redirect()->route('admin.pages.add');
		}
		$id = DB::table('pages')->insertGetId($data);

		if ($id) {
			Session::flash('status', 'success');
			Session::flash('notify', 'Thêm trang mới thành công!');
			MAC::xml();
			return redirect()->route('admin.pages.edit', ['id' => $id]);
		} else {
			Session::flash('status', 'danger');
			Session::flash('notify', 'Thêm trang mới không thành công!');
			return redirect()->route('admin.pages.index');
		}
	}

	public function edit($id) {
		$page = DB::table('pages')->where('id', $id)->first();
		if (isset($page)) {
			$data['data'] = $page;
			$data['list'] = DB::table('pages')->select('id', 'name', 'parent_id')->get();
			if ($page->type == 'post') {
				$data['category'] = DB::table('category')->select('id', 'name')->get();
			} else if ($page->type == 'product') {
				$data['catalog'] = DB::table('catalog')->select('id', 'name')->get();
			}
			if ($page->child) {
				$data['child'] = explode(',', $page->child);
			}
			return view('admin.pages.edit', $data);
		}
		Session::flash('status', 'danger');
		Session::flash('notify', 'Không có trang này!');
		return redirect()->route('admin.pages.index');
	}

	public function update(Request $request, $id) {
		$page = DB::table('pages')->where('id', $id)->first();
		if (isset($page)) {
			$rules = [
				'name' => 'required|max:200',
			];
			$messages = config('lang.vi');
			$date_time = date('Y-m-d H:i:s');
			$parent_id = (int) $request->parent;
			$location = (!empty($request->location)) ? json_encode($request->location) : json_encode([]);
			if ($request->type == 'product') {
				$html = 'product';
			} else if ($request->type == 'post') {
				$html = 'post';
			} else {
				$html = 'taxonomy';
			}
			$arr_seo = explode(',', $request->str_seo);
			foreach ($arr_seo as $item) {
				if (!empty($item)) {
					$meta[$item] = $request->input($item);
					if (empty($meta[$item])) {
						Session::flash('status', 'danger');
						Session::flash('notify', 'Vui lòng không để trống giá trị thẻ SEO');
						return redirect()->back();
					}
				}
			}
			$data = [
				'name' => $request->name,
				'slug' => Str::slug($request->name, '-'),
				'parent_id' => $parent_id,
				'location' => $location,
				'display' => $request->display,
				'html' => ($request->html) ? $request->html : $html,
				'child' => $request->cate_child,
				'taxonomy' => $request->taxonomy,
				'type' => $request->type,
				'page_title' => $request->page_title,
				'link' => $request->link,
				'content' => $request->content,
				'meta' => (@$meta) ? json_encode($meta) : '',
				'stt' => (int) $request->stt,
				'priority' => (double) $request->priority,
			];
			$this->validate($request, $rules, $messages);
			$function = DB::table('pages')->where('id', $id)->update($data);
			if ($function) {
				if ($parent_id != 0) {
					$arr_page = DB::table('pages')->where('id', $parent_id)->value('child');
					$arr_page = explode(',', $arr_page);
					if (empty($arr_page)) {
						$page_child = [$id];
					} else {
						array_push($arr_page, $id);
						$page_child = $arr_page;
					}
					$page_child = array_unique($page_child);
					$page_child = implode(',', $page_child);
					DB::table('pages')->where('id', $parent_id)->update(['child' => $page_child]);
				}
				Session::flash('status', 'success');
				Session::flash('notify', 'Cập nhật thông tin trang thành công!');
				MAC::xml();
			} else {
				Session::flash('status', 'danger');
				Session::flash('notify', 'Cập nhật thông tin trang không thành công!');
			}
			return redirect()->route('admin.pages.edit', ['id' => $id]);
		}
		Session::flash('status', 'danger');
		Session::flash('notify', 'Không có trang này!');
		return redirect()->route('admin.pages.index');
	}

	public function delete($id) {
		$page = DB::table('pages')->where('id', $id)->first();
		if (isset($page)) {
			if (!empty($page->child)) {
				Session::flash('status', 'success');
				Session::flash('notify', 'Vui lòng di chuyển những trang con ra khỏi trang này rồi hãy xóa!');
				return redirect()->route('admin.pages.index');
			}
			$function = DB::table('pages')->where('id', $id)->delete();
			if ($function) {
				if ($page->parent_id != 0) {
					$arr_page = DB::table('pages')->where('id', $page->parent_id)->value('child');
					$arr_page = explode(',', $arr_page);
					$page_child = [];
					if (in_array($id, $arr_page)) {
						$page_child = array_diff($arr_page, explode(',', $id));
					}
					$page_child = array_unique($page_child);
					$page_child = implode(',', $page_child);
					DB::table('pages')->where('id', $page->parent_id)->update(['child' => $page_child]);
				}
				MAC::xml();
				Session::flash('status', 'success');
				Session::flash('notify', 'Xóa trang thành công!');
			} else {
				Session::flash('status', 'danger');
				Session::flash('notify', 'Xóa trang không thành công!');
			}
			return redirect()->route('admin.pages.index');
		}
		Session::flash('status', 'danger');
		Session::flash('notify', 'Không có trang này!');
		return redirect()->route('admin.pages.index');
	}
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:50|unique:users,name',
            'email'                 => 'required|max:50|unique:users,email',
            'password'              => 'required|min:8|max:16',
            'password_confirmation' => 'same:password',
            'fullname'              => 'required|max:200',
            'phone'                 => 'required|max:12',
            'address'               => 'max:200',
            'company'               => 'max:200',
            'country'               => 'max:200',
            'description'           => 'max:2000',
        ];
    }

    public function messages()
    {
        return config('lang.vi');
    }
}

<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if (Auth::user()) {
            dd(1);
            if (isset($guards)) {
                $roles = json_decode(Auth::user()->roles);
                if (isset($roles)) {
                    foreach ($guards as $value) {
                        if (in_array($value, $roles)) {
                            $login = true;
                            break;
                        }else{
                            $login = false;
                        }
                    }
                    if ($login == false) {
                        return redirect()->back();
                    }
                }else{
                    Auth::logout();
                    return redirect()->route('login');
                }
            }
        }else{
            Auth::logout();
            return redirect()->route('login');
        }
        return $next($request);
    }
}

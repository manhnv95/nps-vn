<?php
Route::prefix('madmin')->name('admin.')->middleware('auth')->group(function() {
    Route::get('dashboard', 'Admin\HomeController@index');

    Route::prefix('users')->name('user.')->group(function() {
        Route::get('/', 'Admin\UserController@index')->name('index');
        Route::get('/add', 'Admin\UserController@create')->name('add');
        Route::post('/add', 'Admin\UserController@store');
        Route::get('/show/{id}', 'Admin\UserController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\UserController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\UserController@update');
        Route::get('/delete/{id}', 'Admin\UserController@delete')->name('delete');
    });

    Route::prefix('administrators')->name('admin.')->group(function() {
        Route::get('/', 'Admin\AdminController@index')->name('index');
        Route::get('/add', 'Admin\AdminController@create')->name('add');
        Route::post('/add', 'Admin\AdminController@store');
        Route::get('/show/{id}', 'Admin\AdminController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\AdminController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\AdminController@update');
        Route::get('/delete/{id}', 'Admin\AdminController@delete')->name('delete');
    });

    Route::prefix('category')->name('category.')->group(function() {
        Route::get('/', 'Admin\CategoryController@index')->name('index');
        Route::get('/add', 'Admin\CategoryController@create')->name('add');
        Route::post('/add', 'Admin\CategoryController@store');
        Route::get('/show/{id}', 'Admin\CategoryController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\CategoryController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\CategoryController@update');
        Route::get('/delete/{id}', 'Admin\CategoryController@delete')->name('delete');
    });

    Route::prefix('catalog')->name('catalog.')->group(function() {
        Route::get('/', 'Admin\CatalogController@index')->name('index');
        Route::get('/add', 'Admin\CatalogController@create')->name('add');
        Route::post('/add', 'Admin\CatalogController@store');
        Route::get('/show/{id}', 'Admin\CatalogController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\CatalogController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\CatalogController@update');
        Route::get('/delete/{id}', 'Admin\CatalogController@delete')->name('delete');
    });

    Route::prefix('posts')->name('post.')->group(function() {
        Route::get('/', 'Admin\PostController@index')->name('index');
        Route::get('/add', 'Admin\PostController@create')->name('add');
        Route::post('/add', 'Admin\PostController@store');
        Route::get('/show/{id}', 'Admin\PostController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\PostController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\PostController@update');
        Route::get('/delete/{id}', 'Admin\PostController@delete')->name('delete');
    });

    Route::prefix('hosting')->name('hosting.')->group(function() {
        Route::get('/', 'Admin\HostingController@index')->name('index');
        Route::get('/add', 'Admin\HostingController@create')->name('add');
        Route::post('/add', 'Admin\HostingController@store');
        Route::get('/show/{id}', 'Admin\HostingController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\HostingController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\HostingController@update');
        Route::get('/delete/{id}', 'Admin\HostingController@delete')->name('delete');
    });

    Route::prefix('vps')->name('vps.')->group(function() {
        Route::get('/', 'Admin\VPSController@index')->name('index');
        Route::get('/add', 'Admin\VPSController@create')->name('add');
        Route::post('/add', 'Admin\VPSController@store');
        Route::get('/show/{id}', 'Admin\VPSController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\VPSController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\VPSController@update');
        Route::get('/delete/{id}', 'Admin\VPSController@delete')->name('delete');
    });

    Route::prefix('dedicated')->name('dedicated.')->group(function() {
        Route::get('/', 'Admin\ServerController@index')->name('index');
        Route::get('/add', 'Admin\ServerController@create')->name('add');
        Route::post('/add', 'Admin\ServerController@store');
        Route::get('/show/{id}', 'Admin\ServerController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\ServerController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\ServerController@update');
        Route::get('/delete/{id}', 'Admin\ServerController@delete')->name('delete');
    });

    Route::prefix('rack')->name('rack.')->group(function() {
        Route::get('/', 'Admin\RackController@index')->name('index');
        Route::get('/add', 'Admin\RackController@create')->name('add');
        Route::post('/add', 'Admin\RackController@store');
        Route::get('/show/{id}', 'Admin\RackController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\RackController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\RackController@update');
        Route::get('/delete/{id}', 'Admin\RackController@delete')->name('delete');
    });

    Route::prefix('colocation')->name('colocation.')->group(function() {
        Route::get('/', 'Admin\ColocationController@index')->name('index');
        Route::get('/add', 'Admin\ColocationController@create')->name('add');
        Route::post('/add', 'Admin\ColocationController@store');
        Route::get('/show/{id}', 'Admin\ColocationController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\ColocationController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\ColocationController@update');
        Route::get('/delete/{id}', 'Admin\ColocationController@delete')->name('delete');
    });

    Route::prefix('nwall')->name('nwall.')->group(function() {
        Route::get('/', 'Admin\NWallController@index')->name('index');
        Route::get('/add', 'Admin\NWallController@create')->name('add');
        Route::post('/add', 'Admin\NWallController@store');
        Route::get('/show/{id}', 'Admin\NWallController@show')->name('show');
        Route::get('/edit/{id}', 'Admin\NWallController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\NWallController@update');
        Route::get('/delete/{id}', 'Admin\NWallController@delete')->name('delete');
    });

    Route::prefix('seo')->name('seo.')->group(function() {
        Route::get('/', 'Admin\SeoController@index')->name('index');
        Route::get('/add', 'Admin\SeoController@create')->name('add');
        Route::post('/add', 'Admin\SeoController@store');
        Route::get('/show/{id}', 'Admin\SeoController@show');
        Route::get('/edit/{id}', 'Admin\SeoController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\SeoController@update');
        Route::get('/delete/{id}', 'Admin\SeoController@delete')->name('delete');
    });

    Route::prefix('pages')->name('pages.')->group(function() {
        Route::get('/', 'Admin\PageController@index')->name('index');
        Route::get('/add', 'Admin\PageController@create')->name('add');
        Route::post('/add', 'Admin\PageController@store');
        Route::get('/show/{id}', 'Admin\PageController@show');
        Route::get('/edit/{id}', 'Admin\PageController@edit')->name('edit');
        Route::post('/edit/{id}', 'Admin\PageController@update');
        Route::get('/delete/{id}', 'Admin\PageController@delete')->name('delete');
    });

    Route::prefix('config')->name('config.')->group(function() {
        Route::get('/', 'Admin\ConfigController@index');
        Route::get('/homepage', 'Admin\ConfigController@show_homepage')->name('show_homepage');
        Route::get('/homepage/{id}', 'Admin\ConfigController@edit_homepage')->name('edit_homepage');
        Route::post('/homepage/{id}', 'Admin\ConfigController@update_homepage');
    });
});
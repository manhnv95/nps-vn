<?php
return [

	// Error for user
	'name.required' 	=> 'Vui lòng nhập Username!',
    'email.required'    => 'Vui lòng nhập Email!',
    'password.required' => 'Vui lòng nhập mật khẩu!',
    'fullname.required' => 'Vui lòng nhập họ và tên!',
    'phone.required'    => 'Vui lòng nhập số điện thoại!',

    'name.max'      	=> 'Username: Vui lòng nhập ít hơn 50 ký tự!',
    'email.max'         => 'Email: Vui lòng nhập ít hơn 50 ký tự!',
    'password.min'      => 'Mật khẩu: Vui lòng nhập nhiều hơn 8 ký tự!',
    'password.max'      => 'Mật khẩu: Vui lòng nhập ít hơn 16 ký tự!',
    'fullname.max'      => 'Họ và tên: Vui lòng nhập ít hơn 200 ký tự!',
    'phone.max'         => 'Số điện thoại: Vui lòng nhập ít hơn 12 ký tự!',
    'address.max'       => 'Địa chỉ: Vui lòng nhập ít hơn 200 ký tự!',
    'company.max'       => 'Công ty: Vui lòng nhập ít hơn 200 ký tự!',
    'country.max'       => 'Quốc gia: Vui lòng nhập ít hơn 200 ký tự!',
    'description.max'   => 'Miêu tả: Vui lòng nhập ít hơn 2000 ký tự!',

    'name.unique'					=> 'Username đã tồn tại',
    'email.unique'					=> 'Email đã tồn tại',
    'password_confirmation.same'   	=> 'Mật khẩu nhập lại chưa khớp!',

    
];
?>
<?php
return[
	[
		'name' 	=> 'header',
		'label'		=> 'Header',
		'data'		=> [
			['name' => 'logo', 'label' => 'Logo', 'type' => 'images']
		]
	],
	[
		'name' 	=> 'slider',
		'label'		=> 'Banner - Slider',
		'data'		=> [
			['name' => 'slider_images_1', 'label' => 'Slide 1', 'type' => 'images'],
			['name' => 'slider_text_1', 'label' => 'Miêu tả slider 1', 'type' => 'text'],
			['name' => 'slider_button_1', 'label' => 'Nút slider 1', 'type' => 'text'],
			['name' => 'slider_button_link_1', 'label' => 'Link slider 1', 'type' => 'link'],

			['name' => 'slider_images_2', 'label' => 'Slide 2', 'type' => 'images'],
			['name' => 'slider_text_2', 'label' => 'Miêu tả slider 2', 'type' => 'text'],
			['name' => 'slider_button_2', 'label' => 'Nút slider 2', 'type' => 'text'],
			['name' => 'slider_button_link_2', 'label' => 'Link slider 2', 'type' => 'link'],

			['name' => 'slider_images_3', 'label' => 'Slide 3', 'type' => 'images'],
			['name' => 'slider_text_3', 'label' => 'Miêu tả slider 3', 'type' => 'text'],
			['name' => 'slider_button_3', 'label' => 'Nút slider 3', 'type' => 'text'],
			['name' => 'slider_button_link_3', 'label' => 'Link slider 3', 'type' => 'link'],
		]
	],
	[
		'name' 	=> 'services',
		'label'		=> 'Dịch vụ của NPS',
		'data'		=> [
			['name' => 'services_title', 'label' => 'Dịch vụ của NPS', 'type' => 'text'],

			['name' => 'slider_images_1', 'label' => 'Slide 1', 'type' => 'images'],
			['name' => 'slider_text_1', 'label' => 'Miêu tả slider 1', 'type' => 'text'],
			['name' => 'slider_button_1', 'label' => 'Nút slider 1', 'type' => 'text'],
			['name' => 'slider_button_link_1', 'label' => 'Link slider 1', 'type' => 'link'],

			['name' => 'slider_images_2', 'label' => 'Slide 2', 'type' => 'images'],
			['name' => 'slider_text_2', 'label' => 'Miêu tả slider 2', 'type' => 'text'],
			['name' => 'slider_button_2', 'label' => 'Nút slider 2', 'type' => 'text'],
			['name' => 'slider_button_link_2', 'label' => 'Link slider 2', 'type' => 'link'],

			['name' => 'slider_images_3', 'label' => 'Slide 3', 'type' => 'images'],
			['name' => 'slider_text_3', 'label' => 'Miêu tả slider 3', 'type' => 'text'],
			['name' => 'slider_button_3', 'label' => 'Nút slider 3', 'type' => 'text'],
			['name' => 'slider_button_link_3', 'label' => 'Link slider 3', 'type' => 'link'],
		]
	],
];
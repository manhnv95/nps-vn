<?php
return [
	'app' => [
		'header' => [
			'label' => 'Header',
			'data' => [
				['name' => 'logo', 'label' => 'Logo', 'type' => 'link'],
				['name' => 'favicon', 'label' => 'Favicon', 'type' => 'link'],
				['name' => 'title_app', 'label' => 'Tiêu đề trang chủ', 'type' => 'label'],
				['name' => 'font_app', 'label' => 'Fonts chữ website', 'type' => 'link'],
				// ['name' => 'link_hosting_price', 'label' => 'Đường dẫn hosting CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_vps_price', 'label' => 'Đường dẫn VPS CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_rentsv_price', 'label' => 'Đường dẫn Thuê server CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_sellsv_price', 'label' => 'Đường dẫn Bán server CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_nwall_price', 'label' => 'Đường dẫn firewall CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_rack_price', 'label' => 'Đường dẫn tủ rack CP.NPS.VN', 'type' => 'link'],
				// ['name' => 'link_colocation_price', 'label' => 'Đường dẫn chỗ đặt sv CP.NPS.VN', 'type' => 'link'],
			],
		],
		'slider' => [
			'label' => 'Banner - Slider',
			'data' => [
				['name' => 'domain_slider', 'label' => 'Domain slider', 'type' => 'link'],
				['name' => 'logo_slider', 'label' => 'Logo Slider', 'type' => 'link'],
				['name' => 'bg_slider', 'label' => 'Background Slider', 'type' => 'link'],

				['name' => 'slider_img_1', 'label' => 'Slide 1', 'type' => 'link'],
				['name' => 'slider_title_1', 'label' => 'Tiêu đề slider 1', 'type' => 'label'],
				['name' => 'slider_text_1', 'label' => 'Miêu tả slider 1', 'type' => 'label'],
				['name' => 'slider_button_1', 'label' => 'Nút slider 1', 'type' => 'text'],
				['name' => 'slider_button_link_1', 'label' => 'Link slider 1', 'type' => 'link'],

				['name' => 'slider_img_2', 'label' => 'Slide 2', 'type' => 'link'],
				['name' => 'slider_title_2', 'label' => 'Tiêu đề slider 1', 'type' => 'label'],
				['name' => 'slider_text_2', 'label' => 'Miêu tả slider 2', 'type' => 'label'],
				['name' => 'slider_button_2', 'label' => 'Nút slider 2', 'type' => 'text'],
				['name' => 'slider_button_link_2', 'label' => 'Link slider 2', 'type' => 'link'],

				['name' => 'slider_img_3', 'label' => 'Slide 3', 'type' => 'link'],
				['name' => 'slider_title_3', 'label' => 'Tiêu đề slider 1', 'type' => 'label'],
				['name' => 'slider_text_3', 'label' => 'Miêu tả slider 3', 'type' => 'label'],
				['name' => 'slider_button_3', 'label' => 'Nút slider 3', 'type' => 'text'],
				['name' => 'slider_button_link_3', 'label' => 'Link slider 3', 'type' => 'link'],
			],
		],
		'footer' => [
			'label' => 'Footer',
			'data' => [
				['name' => 'footer_col1_title', 'label' => 'Tiêu đề cột 1', 'type' => 'label'],
				['name' => 'footer_col1_content', 'label' => 'Nội dung cột 1', 'type' => 'text'],

				['name' => 'footer_col2_title', 'label' => 'Tiêu đề cột 2', 'type' => 'label'],
				['name' => 'footer_col2_content', 'label' => 'Nội dung cột 2', 'type' => 'text'],

				['name' => 'footer_col3_title', 'label' => 'Tiêu đề cột 3', 'type' => 'label'],
				['name' => 'footer_col3_content', 'label' => 'Nội dung cột 3', 'type' => 'text'],

				['name' => 'footer_company_info', 'label' => 'Địa chỉ', 'type' => 'text'],

				['name' => 'footer_img1', 'label' => 'Hình ảnh cuối trang 1', 'type' => 'link'],
				['name' => 'footer_img2', 'label' => 'Hình ảnh cuối trang 2', 'type' => 'link'],
				['name' => 'footer_img3', 'label' => 'Hình ảnh cuối trang 3', 'type' => 'link'],
				['name' => 'footer_img4', 'label' => 'Hình ảnh cuối trang 4', 'type' => 'link'],
				['name' => 'footer_img5', 'label' => 'Hình ảnh cuối trang 5', 'type' => 'link'],
				['name' => 'bct_href', 'label' => 'Bộ công thương', 'type' => 'link'],
			],
		],
	],

	'home' => [
		'services' => [
			'label' => 'Dịch vụ của NPS',
			'data' => [
				['name' => 'services_title', 'label' => 'Dịch vụ của NPS', 'type' => 'label'],

				['name' => 'services_img_1', 'label' => 'Hình minh họa 1', 'type' => 'link'],
				['name' => 'services_title_1', 'label' => 'Tiêu đề panel 1', 'type' => 'label'],
				['name' => 'services_description_1', 'label' => 'Miêu tả panel 1', 'type' => 'text'],

				['name' => 'services_img_2', 'label' => 'Hình minh họa 2', 'type' => 'link'],
				['name' => 'services_title_2', 'label' => 'Tiêu đề panel 2', 'type' => 'label'],
				['name' => 'services_description_2', 'label' => 'Miêu tả panel 2', 'type' => 'text'],

				['name' => 'services_img_3', 'label' => 'Hình minh họa 3', 'type' => 'link'],
				['name' => 'services_title_3', 'label' => 'Tiêu đề panel 3', 'type' => 'label'],
				['name' => 'services_description_3', 'label' => 'Miêu tả panel 3', 'type' => 'text'],

				['name' => 'services_img_4', 'label' => 'Hình minh họa 4', 'type' => 'link'],
				['name' => 'services_title_4', 'label' => 'Tiêu đề panel 4', 'type' => 'label'],
				['name' => 'services_description_4', 'label' => 'Miêu tả panel 4', 'type' => 'text'],
			],
		],
		'support' => [
			'label' => 'Hỗ trợ đa nền tảng',
			'data' => [
				['name' => 'support_title', 'label' => 'Hỗ trợ đa nền tảng', 'type' => 'label'],

				['name' => 'support_img_1', 'label' => 'Hình minh họa 1', 'type' => 'link'],
				['name' => 'support_img_2', 'label' => 'Hình minh họa 2', 'type' => 'link'],
				['name' => 'support_img_3', 'label' => 'Hình minh họa 3', 'type' => 'link'],
				['name' => 'support_img_4', 'label' => 'Hình minh họa 4', 'type' => 'link'],
			],
		],
		'hosting' => [
			'label' => 'Hosting',
			'data' => [
				['name' => 'hosting_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'hosting_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'hosting_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'hosting_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'hosting_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'hosting_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'hosting_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'vps' => [
			'label' => 'VPS',
			'data' => [
				['name' => 'vps_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'vps_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'vps_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'vps_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'vps_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'vps_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'vps_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'rentsv' => [
			'label' => 'Thuê Server',
			'data' => [
				['name' => 'rentsv_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'rentsv_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'rentsv_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'rentsv_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'rentsv_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'rentsv_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'rentsv_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'nwall' => [
			'label' => 'Firewall',
			'data' => [
				['name' => 'nwall_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'nwall_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'nwall_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'nwall_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'nwall_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'nwall_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'nwall_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'sellsv' => [
			'label' => 'Bán Server',
			'data' => [
				['name' => 'sellsv_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'sellsv_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'sellsv_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'sellsv_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'sellsv_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'sellsv_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'sellsv_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'colocation' => [
			'label' => 'Chỗ đặt Server',
			'data' => [
				['name' => 'colocation_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'colocation_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'colocation_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'colocation_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'colocation_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'colocation_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'colocation_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
		'rack' => [
			'label' => 'Tủ Rack',
			'data' => [
				['name' => 'rack_title', 'label' => 'Tiêu đề', 'type' => 'label'],
				['name' => 'rack_panel_title', 'label' => 'Thông số kỹ thuật', 'type' => 'label'],
				['name' => 'rack_panel_button', 'label' => 'Nội dung nút Mua ngay', 'type' => 'label'],
				['name' => 'rack_panel_button_link', 'label' => 'Đường dẫn nút Mua ngay', 'type' => 'link'],

				['name' => 'rack_des_title', 'label' => 'Tiêu đề phần miêu tả', 'type' => 'label'],
				['name' => 'rack_des_text', 'label' => 'Nội dung phần miêu tả', 'type' => 'text'],
				['name' => 'rack_des_img', 'label' => 'Hình ảnh phần miêu tả', 'type' => 'link'],
			],
		],
	],
];